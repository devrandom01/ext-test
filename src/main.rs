use first::f1;

fn main() {
    println!("Hello, world!");
    f1();
}

mod tests {
    use first::{set_global, tests::test_f1};

    #[test]
    fn test_first_crate() {
        set_global("in second".to_string());
        test_f1();
    }

    #[test]
    fn test_other() {
        assert_eq!(2 + 2, 4);
    }
}
