use test_macro::xtest;
use once_cell::sync::OnceCell;

pub static GLOBAL: OnceCell<String> = OnceCell::new();

fn get_global() -> String {
	GLOBAL.get_or_init(|| "default".to_string()).clone()
}

/// Infallible setter, to allow multiple tests to set the global value.
/// Assume that all the set attempts use the same value.
pub fn set_global(s: String) {
	let _ = GLOBAL.set(s);
}

pub fn f1() {
	println!("f1 GLOBAL={}", get_global());
}

#[xtest(feature = "xtest")]
pub mod tests {
	use super::*;

	#[xtest]
	pub fn test_f1() {
		f1();
	}

	#[xtest]
	#[ignore]
	pub fn test_f2() {
		println!("f2 GLOBAL={}", get_global());
	}
}
